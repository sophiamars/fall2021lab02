//Sophia Marshment - 2038832

public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bicycles = new Bicycle[4];

        bicycles[0] = new Bicycle("The Bike Supply Store", 20, 45);
        bicycles[1] = new Bicycle("The Rubber Tire", 23, 40);
        bicycles[2] = new Bicycle("Bike Riot", 19, 35);
        bicycles[3] = new Bicycle("Cycling World", 27, 50);

        for (int x = 0; x < bicycles.length; x++) {
            System.out.println(bicycles[x]);
        }
    }
}
