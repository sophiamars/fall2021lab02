//Sophia Marshment - 2038832

public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumberGears() {
        return this.numberGears;
    }

    public double maxpeed() {
        return this.maxSpeed;
    }

    public String toString() {
        String text = "Manufacturer: "+this.manufacturer+", Number of Gears: "+this.numberGears+", Max Speed: "+this.maxSpeed;
        return text;
    }
}